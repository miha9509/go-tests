package usecase

import (
	"gitlab.com/miha9509/go-tests/article"
	"gitlab.com/miha9509/go-tests/models"
)

type ArticleUsecase struct {
	ArticleRepo article.Repository
}

func NewArticleUsecase(ar article.Repository) ArticleUsecase {
	return ArticleUsecase{ar}
}

func (au ArticleUsecase) GetArticles() ([]*models.Article, error) {
	return au.ArticleRepo.GetArticles()
}
