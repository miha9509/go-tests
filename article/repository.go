package article

import "gitlab.com/miha9509/go-tests/models"

type Repository interface {
	GetArticles() ([]*models.Article, error)
}
