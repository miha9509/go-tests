package postgres

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"gitlab.com/miha9509/go-tests/models"
)

type Article struct {
	ID           int
	Header       string
	Body         string
	DateCreated  time.Time
	DateModified time.Time
}

type ArticleRepository struct {
	db *pgxpool.Pool
}

func NewArticleRepository(db *pgxpool.Pool) *ArticleRepository {
	return &ArticleRepository{db}
}

func (ar *ArticleRepository) GetArticles() ([]*models.Article, error) {
	rows, err := ar.db.Query(context.Background(),
		`SELECT 
			id,
			header,
			body,
			date_created,
			date_modified
		FROM 
			"articles"`)
	if err != nil {
		return nil, errors.Wrap(err, "get articles")
	}
	defer rows.Close()

	articles := []*Article{}

	for rows.Next() {
		var a Article

		if err := rows.Scan(
			&a.ID,
			&a.Header,
			&a.Body,
			&a.DateCreated,
			&a.DateModified,
		); err != nil {
			return nil, errors.Wrap(err, "scan article")
		}

		articles = append(articles, &a)
	}

	return toArticles(articles), nil
}

func toArticle(a *Article) *models.Article {
	return &models.Article{
		Header:     a.Header,
		Body:       a.Body,
		CreatedOn:  a.DateCreated,
		ModifiedOn: a.DateModified,
	}
}

func toArticles(as []*Article) []*models.Article {
	out := make([]*models.Article, len(as))

	for i, a := range as {
		out[i] = toArticle(a)
	}

	return out
}
