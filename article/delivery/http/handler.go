package http

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/miha9509/go-tests/article"
	"gitlab.com/miha9509/go-tests/models"
)

type Article struct {
	Header string
	Body   string
}

type Handler struct {
	useCase article.ArticleUsecase
}

func NewHandler(uc article.ArticleUsecase) *Handler {
	return &Handler{uc}
}

type getResponse struct {
	Articles []*Article `json:"articles"`
}

func (h *Handler) Get(c *gin.Context) {
	articles, err := h.useCase.GetArticles()
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, &getResponse{
		Articles: toArticles(articles),
	})
}

func toArticles(bs []*models.Article) []*Article {
	out := make([]*Article, len(bs))

	for i, b := range bs {
		out[i] = toArticle(b)
	}

	return out
}

func toArticle(b *models.Article) *Article {
	return &Article{
		Header: b.Header,
		Body:   b.Body,
	}
}
