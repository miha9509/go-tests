package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/miha9509/go-tests/article"
)

func RegisterHTTPEndpoints(router *gin.RouterGroup, uc article.ArticleUsecase) {
	h := NewHandler(uc)

	bookmarks := router.Group("/articles")
	{
		bookmarks.GET("", h.Get)
	}
}
