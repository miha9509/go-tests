package article

import "gitlab.com/miha9509/go-tests/models"

type ArticleUsecase interface {
	GetArticles() ([]*models.Article, error)
}
