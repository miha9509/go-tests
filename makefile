.PHONY:
.SILENT:

build: ## Build application
	go build -o ./bin/main -ldflags "-w -s -X main.built=`date -u +%Y%m%d.H%M%S` -X main.commit=`git rev-parse --short HEAD`" -v cmd/main.go

run: ## Build and run application
	./bin/main ./etc/config.yaml

lint: ## Lint code
	golangci-lint run

test: ## Complete authotests
	go test ./...

dockerversion: ## Check docker version
	docker --version
	docker-compose --version
	docker-machine --version

dockerpullpg: ## Pull docker postgres image
	docker pull postgres

help: ## Display this help screen
	grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

rundocker:
	sudo chmod 666 /var/run/docker.sock

composeup: ## Start docker
	docker-compose up -d

composedown: ## Stop docker
	docker-compose down

migrationsinit:
	go run ./migrations/*.go init

migrationsup:
	go run ./migrations/*.go

migrationsreset:
	go run ./migrations/*.go reset

migrationsdown:
	go run ./migrations/*.go down

uppg: ## Create tables in pg and fill it
	make migrationsinit
	make migrationsup
