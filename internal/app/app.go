package app

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"

	"gitlab.com/miha9509/go-tests/article"
	articlehttp "gitlab.com/miha9509/go-tests/article/delivery/http"
	articlepg "gitlab.com/miha9509/go-tests/article/repository/postgres"
	articleuk "gitlab.com/miha9509/go-tests/article/usecase"
	"gitlab.com/miha9509/go-tests/internal/config"
)

type App struct {
	articleUC article.ArticleUsecase
}

func (a *App) Init(c *config.Config) error {
	pg, err := initPostgres(c.DB)
	if err != nil {
		return err
	}

	articleRepo := articlepg.NewArticleRepository(pg)

	a.articleUC = articleuk.NewArticleUsecase(articleRepo)

	return nil
}

func (a *App) Run() {
	// Init gin handler
	router := gin.Default()
	router.Use(
		gin.Recovery(),
		gin.Logger(),
	)

	// API endpoints
	api := router.Group("/api")

	articlehttp.RegisterHTTPEndpoints(api, a.articleUC)

	router.Run(":8080")
}

// initPostgres return postgres.
func initPostgres(conf *config.Postgres) (*pgxpool.Pool, error) {
	DBUrl := fmt.Sprintf(
		"postgresql://%s:%s@%s:%d/%s?sslmode=%s&pool_max_conns=%d",
		conf.User,
		conf.Password,
		conf.Host,
		conf.Port,
		conf.Dbname,
		conf.Sslmode,
		conf.Poolmaxconns)

	db, err := pgxpool.Connect(context.Background(), DBUrl)
	if err != nil {
		return nil, errors.Wrap(err, "make pg connect")
	}

	return db, nil
}
