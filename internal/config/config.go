package config

import (
	"io/ioutil"
	"os"

	"github.com/caarlos0/env"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Config Общая структура конфига.
type Config struct {
	DB *Postgres
}

// Config type.
type Postgres struct {
	Host         string `env:"DB_HOST"`
	Port         int    `env:"DB_PORT"`
	Dbname       string `env:"DB_NAME"`
	User         string `env:"DB_USER"`
	Password     string `env:"DB_PASSWORD"`
	Sslmode      string `env:"DB_SSLMODE"`
	Poolmaxconns int    `env:"DB_POOLMAXCONNS"`
}

// GetConfig return config.
func GetConfig(path string) (*Config, error) {
	// Filepath in os.Args.
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	// Get config from YAML.
	if config, err := getConfigFromFile(path); err == nil {
		return config, nil
	}
	// Get config from ENV.
	if config, err := getConfigFromENV(); err == nil {
		return config, nil
	}

	return nil, errors.New("no envs and yaml")
}

func getConfigFromFile(path string) (*Config, error) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.Wrap(err, "read config file")
	}

	config := new(Config)

	err = yaml.Unmarshal(content, &config)
	if err != nil {
		return nil, errors.Wrap(err, "unmarshal config file from yaml")
	}

	return config, nil
}

func getConfigFromENV() (*Config, error) {
	db := Postgres{}

	if err := env.Parse(&db); err != nil {
		return nil, errors.Wrap(err, "unmarshal db config from env")
	}

	config := Config{
		DB: &db,
	}

	return &config, nil
}
