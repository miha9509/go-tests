package models

import "time"

type User struct {
	UserName   string
	CreatedOn  time.Time
	ModifiedOn time.Time
}
