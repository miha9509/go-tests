package models

import "time"

type Article struct {
	Header     string
	Body       string
	CreatedOn  time.Time
	ModifiedOn time.Time
}
