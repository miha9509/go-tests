module gitlab.com/miha9509/go-tests

go 1.16

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/go-pg/migrations/v8 v8.1.0
	github.com/go-pg/pg/v10 v10.10.5
	github.com/jackc/pgx/v4 v4.13.0
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v2 v2.4.0
)
