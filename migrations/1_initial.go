package main

import (
	"fmt"

	"github.com/go-pg/migrations/v8"
)

// nolint: gochecknoinits
func init() {
	migrations.MustRegisterTx(func(db migrations.DB) error {
		fmt.Println("creating table my_table...")

		if _, err := db.Exec(`
		CREATE TABLE users (
			id SERIAL PRIMARY KEY,
			username TEXT NOT NULL UNIQUE,
			date_created TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
			date_modified TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
		)`); err != nil {
			return err
		}

		if _, err := db.Exec(`CREATE TABLE articles (
			id SERIAL PRIMARY KEY,
			header TEXT NOT NULL UNIQUE,
			body TEXT NOT NULL,
			date_created TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
			date_modified TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
		)`); err != nil {
			return err
		}

		return nil
	}, func(db migrations.DB) error {
		fmt.Println("dropping table my_table...")

		if _, err := db.Exec(`DROP TABLE users`); err != nil {
			return err
		}

		if _, err := db.Exec(`DROP TABLE articles`); err != nil {
			return err
		}

		return nil
	})
}
