package main

import (
	"fmt"

	"github.com/go-pg/migrations/v8"
)

// nolint: gochecknoinits
func init() {
	migrations.MustRegisterTx(func(db migrations.DB) error {
		fmt.Println("insert into table users...")

		if _, err := db.Exec(`
		INSERT INTO users (username) VALUES
		('mikhail')
		`); err != nil {
			return err
		}

		fmt.Println("insert into table articles...")

		if _, err := db.Exec(`
		INSERT INTO articles (header, body) VALUES
		('first header', 'test body'),
		('second header', 'test body')
			`); err != nil {
			return err
		}

		return nil
	}, func(db migrations.DB) error {
		fmt.Println("delete from table users...")

		if _, err := db.Exec(`DELETE FROM users`); err != nil {
			return err
		}

		fmt.Println("delete from table articles...")

		if _, err := db.Exec(`DELETE FROM articles`); err != nil {
			return err
		}

		return nil
	})
}
