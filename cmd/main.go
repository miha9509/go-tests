package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/miha9509/go-tests/internal/app"
	"gitlab.com/miha9509/go-tests/internal/config"
)

const configPath = "./../etc/config.yaml"

func main() {
	application := app.App{}

	conf, err := config.GetConfig(configPath)
	if err != nil {
		log.Fatal(err)
	}

	if err := application.Init(conf); err != nil {
		log.Fatal(err)
	}

	defer print("hello")

	application.Run()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	fmt.Print("\napp stopped")
}
